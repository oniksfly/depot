class MigratePricesFromProducts < ActiveRecord::Migration
  def up
    LineItem.where(:price => nil).each do |line_item|
      line_item.update_attributes(price: line_item.product.price)
    end
  end
  def down
    LineItem.all.each do |line_item|
      line_item.update_attributes(price: nil)
    end
  end
end
