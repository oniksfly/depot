class User < ActiveRecord::Base
  validates :name, presence: true, uniqueness: true
  after_destroy :ensure_admin_remains
  has_secure_password

  private
  def ensure_admin_remains
    raise 'Can\'t delete last user' if User.count.zero?
  end
end
