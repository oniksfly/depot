class StoreController < ApplicationController
  include CurrentCart

  before_action :count_index, only: [:index]
  before_action :set_cart
  skip_before_action :authorise

  def index
    if params[:set_locale]
      redirect_to store_url(locale: params[:set_locale])
    else
      @visit_times = session[:counter]
      @products = Product.order(:title)
    end
  end
end
