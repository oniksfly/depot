require 'test_helper'

class ProductTest < ActiveSupport::TestCase
  fixtures :products

  test 'product attributes must be not empty' do
    product = Product.new
    assert product.invalid?
    assert product.errors[:title].any?
    assert product.errors[:description].any?
    assert product.errors[:price].any?
    assert product.errors[:image_url].any?
  end


  test 'product price must be positive' do
    product = Product.new(title: 'Test product', description: 'Test it', image_url: 'ttefwty.jpg')
    product.price = -1
    assert product.invalid?
    assert_equal ['must be greater than or equal to 0.01'], product.errors[:price]

    product.price = 0
    assert product.invalid?
    assert_equal ['must be greater than or equal to 0.01'], product.errors[:price]

    product.price = 1
    assert product.valid?
  end


  test 'image url' do
    ok = %w{ fedd.gif test.jpg test.JpG tTest.pnG ^tyshg~_.//sd\.gif }
    bad = %w{ fer.doc gif.gif/more test.jpg.php }

    ok.each do |name|
      assert new_product(name).valid?, "#{name} should be valid"
    end

    bad.each do |name|
      assert new_product(name).invalid?, "#{name} should be invalid"
    end
  end

  test 'product is not valid without unique title' do
    product = Product.new(title: products(:ruby).title, description: 'Some words', price: 1.2, image_url: 'test.gif')
    assert product.invalid?
    assert_equal [I18n.translate('errors.messages.taken')], product.errors[:title]
  end

  test 'product title must be longer then 10 symbols' do
    product = Product.new(title: 'Shrt ttl', description: 'Descriptg', price: 45.34, image_url: 'were.png')
    assert product.invalid?
    assert_equal ['Minimum 10 symbols'], product.errors[:title]
  end


  def new_product(image_url)
    Product.new(title: 'Book Title', description: 'Book descr', price: 1.99, image_url: image_url)
  end
end
